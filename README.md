<img align="left" src="http://8sync.gitlab.io/gfx/programs/mors_logo128.png">

## mors

An interpreter for the mors language, written in plain C. 

For details on how to write mors code (get it?) visit the wiki.
### Usage
- no arguments - console
- -f *file* [ARGS] - execute a file, with optional command line arguments
- -v - show version
- -h or --help - show help
