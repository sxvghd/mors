#include <stdlib.h>
#include "mors_helpers.h"
void mors_print_help()
{
	printf("mors " MORS_VERSION " by 8sync\nArguments:\n -f <file> [ARGS] - execute a mors file (.mors), with optional arguments\n -v - show the version\n -h or --help - show this help\n");
}
void mors_print_error(char *msg)
{
	#ifdef _WIN32
	printf("[ERROR]: %s\n",msg);
	#else
	printf(COLOR_RED);
	printf("[ERROR]: %s\n",msg);
	printf(COLOR_RESET);
	#endif	
}
void mors_print_warning(char *msg)
{
	#ifdef _WIN32
	printf("[WARNING]: %s\n",msg);
	#else
	printf(COLOR_YELLOW);
	printf("[WARNING]: %s\n",msg);
	printf(COLOR_RESET);
	#endif	

}
void mors_print_console_msg()
{
	#ifdef _WIN32
	printf("mors " MORS_VERSION "\n");
	#else
	printf(COLOR_GREEN "mors " COLOR_RED MORS_VERSION COLOR_RESET"\n");
	#endif
}
void mors_print_console_prompt(bool isArgument)
{
	#ifdef _WIN32
	if(!isArgument)
		printf("mors");
	else
		printf("mors mors");
	printf(" > ");
	#else
	if(!isArgument)
		printf(COLOR_GREEN "mors" COLOR_RESET " > ");
	else
		printf(COLOR_GREEN "mors mors" COLOR_RESET " > ");
	#endif
}
void mors_initialize_memory(int **memory_tape, int start_init_pos, int end_init_pos)
{
	for(int i = start_init_pos; i<=end_init_pos; i++)
		(*memory_tape)[i] = 0;
	return;
}
void mors_resize_memory(int **memory_tape, int *memory_length, int wanted_memory_length)
{
	*memory_tape = realloc(*memory_tape,wanted_memory_length*sizeof(int*));
	mors_initialize_memory(memory_tape,*memory_length,wanted_memory_length-1);
	*memory_length = wanted_memory_length;
	return;
}
int mors_count_input_morses(char *input)
{
	int count = 0;
	while((input = strstr(input,"mors")) != NULL)
	{
		input++;
		count++;
	}
	return count;
}
char* mors_read_line(FILE *file, int length)
{
	static char *line;
	line = malloc(length*sizeof(char));
	for(int i = 0;i<length;i++)
		line[i] = fgetc(file);
	return line;
}
