#ifndef MORS_OPCODES_H
#define MORS_OPCODES_H
int mors_exec_opcode(int opcode, int argument, int **memory_tape, int *memory_length, int *cursor, int *selection);
#endif
