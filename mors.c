#ifdef _WIN32
#include <windows.h>
#include "win32\win_tools.h"
#endif
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mors_helpers.h"
#include "mors_opcodes.h"
int main(int argc, char *argv[])
{
	int memory_length = 1;
	int *memory_tape = calloc(memory_length,sizeof(int));
	int cursor = 0;
	int selection = 0;
	int if_indent_count = 0;
	int mors_opcode = 0;
	int mors_parameter = 0;
	if(argc>1)
	{
		if(strstr(argv[1],"-h")!=NULL)
			mors_print_help();
		else if(strstr(argv[1],"-v")!=NULL)
			mors_print_console_msg();
		else if(strstr(argv[1],"-f")!=NULL)
		{
			int mors_current_arg = 3;
			mors_is_console = false;
			//this is where the fun stuff happens
			FILE *file;
			char *line = NULL;
			size_t len = 0;
			#ifdef _WIN32
			file = fopen(argv[2],"rb");
			#else
			file = fopen(argv[2],"r");
			#endif
			if(file == NULL)
			{
				mors_print_error("Error loading file (file not found)");
				free(line);
				free(memory_tape);
				return 1;
			}
			if(strstr(argv[2],".mors")==NULL)
				mors_print_warning("Input file has wrong extension (not .mors)");
			//get nr of lines
			int lines = 0;
			while(!feof(file))
			{
				char ch;
  				ch = fgetc(file);
  				if(ch == '\n')
    					lines++;
			}
			rewind(file);
			//get lines' beginnings positions
			long line_pos[lines];
			lines = 1;
			line_pos[0] = ftell(file);
			while(!feof(file))
			{
				char ch;
				ch = fgetc(file);
				if(ch == '\n')
				{
					line_pos[lines] = ftell(file);
					lines++;
				}
			}
			rewind(file);
			lines = 0;
			while(!feof(file))
			{
				line = mors_read_line(file,line_pos[lines+1]-line_pos[lines]);
				lines++;	
				mors_opcode = mors_count_input_morses(line);
				//get an additional argument for selected opcodes
				if((mors_opcode > 0 && mors_opcode < 5) || mors_opcode == 7)
				{
					if(!feof(file))
					{
						line = mors_read_line(file,line_pos[lines+1]-line_pos[lines]);
						lines++;			
						mors_parameter = mors_count_input_morses(line);	
					}
					else
					{
						mors_print_error("Expected an opcode argument");
						free(line);
						free(memory_tape);
						return 2;
					}
				}
				switch(mors_opcode)
				{
					case 0:
						free(line);
						free(memory_tape);
						return 0;
					 case 6:
                        if((argc>3)&&(mors_current_arg<argc))
                        {
                            //TODO: Move this piece of code to a separate helper function and use it in mors_opcode_get
                            bool isNumber = true;
                            int length = strlen(argv[mors_current_arg]);
                            for (int i=0;i<length;i++)
                                if (!isdigit(argv[mors_current_arg][i]))
                                    isNumber = false;
                            if(isNumber)
                            {
                                int power = length;
                                int number = 0;
                                number = atoi(argv[mors_current_arg]);
                                memory_tape[cursor] = number;
                            }
                            else
                            {
                                if(cursor+length>memory_length)
                                    mors_resize_memory(&memory_tape,&memory_length,cursor+length+1);
                                for(int i=0;i<length;i++)
                                    memory_tape[cursor+i] = argv[mors_current_arg][i];
                            }
                            mors_current_arg++;
                        }
                        else
                            mors_exec_opcode(mors_opcode,mors_parameter,&memory_tape,&memory_length,&cursor,&selection);
                        break;
					case 7:
						lines = mors_parameter;
						fseek(file,line_pos[mors_parameter],SEEK_SET);
						break;
					case 8:
						if(memory_tape[cursor] != memory_tape[selection])
						{
							if_indent_count++;
							while(if_indent_count != 0)
							{
								line = mors_read_line(file,line_pos[lines+1]-line_pos[lines]);
								lines++;	
								mors_opcode = mors_count_input_morses(line);
								if((mors_opcode > 0 && mors_opcode < 5) || mors_opcode == 7)
								{
									line = mors_read_line(file,line_pos[lines+1]-line_pos[lines]);
									lines++;	
								}
								else if(mors_opcode > 7 && mors_opcode < 11)
									if_indent_count++;
								else if(mors_opcode == 11)
									if_indent_count--;
							}
						}
						break;
					case 9:
						if(memory_tape[cursor] < memory_tape[selection] || memory_tape[cursor] == memory_tape[selection])
						{
							if_indent_count++;
							while(if_indent_count != 0)
							{
								line = mors_read_line(file,line_pos[lines+1]-line_pos[lines]);
								lines++;	
								mors_opcode = mors_count_input_morses(line);
								if((mors_opcode > 0 && mors_opcode < 5) || mors_opcode == 7)
								{
									line = mors_read_line(file,line_pos[lines+1]-line_pos[lines]);
									lines++;	
								}
								else if(mors_opcode > 7 && mors_opcode < 11)
									if_indent_count++;
								else if(mors_opcode == 11)
									if_indent_count--;
							}
						}
						break;
					case 10:
						if(memory_tape[cursor] > memory_tape[selection] || memory_tape[cursor] == memory_tape[selection])
						{
							if_indent_count++;
							while(if_indent_count != 0)
							{
								line = mors_read_line(file,line_pos[lines+1]-line_pos[lines]);
								lines++;	
								mors_opcode = mors_count_input_morses(line);
								if((mors_opcode > 0 && mors_opcode < 5) || mors_opcode == 7)
								{
									line = mors_read_line(file,line_pos[lines+1]-line_pos[lines]);
									lines++;	
								}
								else if(mors_opcode > 7 && mors_opcode < 11)
									if_indent_count++;
								else if(mors_opcode == 11)
									if_indent_count--;
							}
						}
						break;

					//implement if statements
					default:
						mors_exec_opcode(mors_opcode,mors_parameter,&memory_tape,&memory_length,&cursor,&selection);
						break;
				}	
			}
		}	
	}
	else
	{
		mors_is_console = true;
		mors_print_console_msg();
		#ifndef _WIN32
		system("stty cbreak -echo");
		#endif
		while(true)
		{
			mors_print_console_prompt(false);	
			while(getchar()!=RETURN_CHAR)
			{
				mors_opcode++;
				printf("mors ");
			}
			printf("\n");
			//check opcodes, load (if necessary) argument, process & output
			if(mors_opcode > 0 && mors_opcode < 5)
			{
				mors_print_console_prompt(true);
				while(getchar()!=RETURN_CHAR)
				{
					mors_parameter++;
					printf("mors ");
				}
				printf("\n");
				mors_exec_opcode(mors_opcode,mors_parameter,&memory_tape,&memory_length,&cursor,&selection);
			}
			else if(mors_opcode == 0)
				break;
			else
				mors_exec_opcode(mors_opcode,mors_parameter,&memory_tape,&memory_length,&cursor,&selection);
			mors_opcode = 0;
			mors_parameter = 0;	
		}
		#ifndef _WIN32
		system("stty cooked echo");
		#endif
	}
	free(memory_tape);
	return 0;
}
