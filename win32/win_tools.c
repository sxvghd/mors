#include <windows.h>
#include <stdbool.h>
bool getChar(TCHAR *ch)
{
  bool    ret = false;

  HANDLE  stdIn = GetStdHandle(STD_INPUT_HANDLE);

  DWORD   saveMode;
  GetConsoleMode(stdIn, &saveMode);
  SetConsoleMode(stdIn, ENABLE_PROCESSED_INPUT);

  if (WaitForSingleObject(stdIn, INFINITE) == WAIT_OBJECT_0)
  {
    DWORD num;
    ReadConsole(stdIn, ch, 1, &num, NULL);

    if (num == 1) ret = true;
  }

  SetConsoleMode(stdIn, saveMode);

  return(ret);
}

char getTheChar(void)
{
  TCHAR ch = 0;
  getChar(&ch);
  //char retval = ((char)ch);
  return ch;
}



