cc = gcc
all:
	$(cc) -lm -o mors mors.c mors_helpers.c mors_helpers.h mors_opcodes.c mors_opcodes.h
windows:
	$(cc) -std=c99 -lm -o mors.exe mors.c mors_helpers.c mors_helpers.h mors_opcodes.c mors_opcodes.h win32/win_tools.h win32/win_tools.c
debug:
	$(cc) -g -lm -o mors mors.c mors_helpers.c mors_helpers.h mors_opcodes.c mors_opcodes.h
android:
	#tested on termux
	clang -lm mors.c mors_helpers.c mors_helpers.h mors_opcodes.c mors_opcodes.h
	mv a.out mors
