#ifndef MORS_HELPERS_H
#define MORS_HELPERS_H
#define MORS_VERSION "0.2"
#ifdef _WIN32
#define RETURN_CHAR '\r'
#else
#define COLOR_RED "\x1b[31m"
#define COLOR_GREEN "\x1b[32m"
#define COLOR_YELLOW "\x1b[33m"
#define COLOR_RESET "\x1b[0m"
#define RETURN_CHAR '\n'
#endif
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
bool mors_is_console;
void mors_print_help();
void mors_print_error(char *msg);
void mors_print_warning(char *msg);
void mors_print_console_msg();
void mors_print_console_prompt(bool isArgument);
void mors_initialize_memory(int **memory_tape, int start_init_pos, int end_init_pos);
void mors_resize_memory(int **memory_tape, int *memory_length, int wanted_memory_length);
int mors_count_input_morses(char *input);
char* mors_read_line(FILE *file, int length);
#endif
