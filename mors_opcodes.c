#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "mors_opcodes.h"
#include "mors_helpers.h"
int mors_opcode_add(int argument, int **memory_tape, int cursor, int selection)
{
	if(argument == 0)
		(*memory_tape)[cursor]+=(*memory_tape)[selection];
	else
		(*memory_tape)[cursor]+=argument;
	return 0;
}
int mors_opcode_sub(int argument, int **memory_tape, int cursor, int selection)
{
	if(argument == 0)
		(*memory_tape)[cursor]-=(*memory_tape)[selection];
	else
		(*memory_tape)[cursor]-=argument;
	return 0;
}
int mors_opcode_sel(int argument, int *selection)
{
	*selection = argument;
	return 0;	
}
int mors_opcode_mov(int argument, int *cursor)
{
	*cursor = argument;
	return 0;
}
int mors_opcode_prnt(int *memory_tape, int cursor, int selection)
{
	if(cursor > selection)
	{
		for(int i = selection;i<=cursor;i++)
			printf("%c",memory_tape[i]);
	}
	else 
	{
		if(cursor == selection)
			printf("%d\n",memory_tape[cursor]);
		else
		{
			for(int i = cursor;i<=selection;i++)
				printf("%c",memory_tape[i]);
		}
	}
}
int mors_opcode_get(int **memory_tape, int *memory_length, int cursor)
{
	#ifndef _WIN32
	if(mors_is_console)
		system("stty cooked echo");
	#endif
	//fixme
	char *input = malloc(1000*sizeof(char));
    	int length,i; 
    	scanf ("%s", input);
    	length = strlen (input);
	bool isNumber = true;
    	for (i=0;i<length;i++)
        	if (!isdigit(input[i]))
        		isNumber = false;
	if(isNumber)
	{
		int power = length;
		int number = 0;
		number = atoi(input);		
		(*memory_tape)[cursor] = number;
	}
	else
	{
		if(cursor+length>*memory_length)
			mors_resize_memory(memory_tape,memory_length,cursor+length+1);
		for(i=0;i<length;i++)
			(*memory_tape)[cursor+i] = input[i];

	}	
	free(input);
	#ifndef _WIN32
	if(mors_is_console)
	{
		system("stty cbreak -echo");
		scanf("\n");
	}
	#endif	
}
//testme
int mors_opcode_sh(int *memory_tape, int cursor, int selection)
{
	int size = 0;
	char *cmd;
	if(cursor > selection)
	{
		size = cursor - selection;
		cmd = malloc(size*sizeof(char));
		for(int i = 0;i<=size;i++)
			cmd[i] = memory_tape[selection+i];
	}
	else
	{
		size = selection - cursor;
		cmd = malloc(size*sizeof(char));
		for(int i = 0;i<=size;i++)
			cmd[i] = memory_tape[cursor+i];
	}
	system(cmd);
	free(cmd);
}
int mors_exec_opcode(int opcode, int argument, int **memory_tape, int *memory_length, int *cursor, int *selection)
{
	if(opcode == 3 || opcode == 4)
	{
		if(opcode == 3)
			mors_opcode_sel(argument, selection);
		else
			mors_opcode_mov(argument, cursor);
		if(*cursor >= *memory_length)
			mors_resize_memory(memory_tape,memory_length,*cursor+1);
		if(*selection >= *memory_length)
			mors_resize_memory(memory_tape,memory_length,*selection+1);	
	}
	else
	{
		switch(opcode)
		{
			case 1:
				mors_opcode_add(argument, memory_tape, *cursor, *selection);
				break;
			case 2:
				mors_opcode_sub(argument, memory_tape, *cursor, *selection);
				break;
			case 5:
				mors_opcode_prnt(*memory_tape, *cursor, *selection);
				break;
			case 6:
				mors_opcode_get(memory_tape, memory_length, *cursor);
				break;
			case 12:
				mors_opcode_sh(*memory_tape, *cursor, *selection);
				break;	
		}
	}
}

